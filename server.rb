require 'bundler'
Bundler.require
require './app'

EM.run do
  @channel = EM::Channel.new
  @subscriptions = {}

  EM::PeriodicTimer.new(1) do
    @channel << rand(1000).to_s
  end

  EM::WebSocket.start(:host => '0.0.0.0', :port => 3001) do |ws|
    ws.onopen do
      subscriber_id = @channel.subscribe do |msg|
        ws.send msg
      end
      @pry = Pry.new
      @pry.print = proc do |_, value|
        ws.send(JSON.dump({:value => value}))
      end
      @subscriptions[ws.signature] = subscriber_id
    end

    ws.onmessage do |msg|
      @pry.eval(msg)
    end

    ws.onclose do
      @channel.unsubscribe @subscriptions[ws.signature]
      @subscriptions.delete ws.signature
    end
  end

  # You could also use Rainbows! instead of Thin.
  # Any EM based Rack handler should do.
  Thin::Server.start App, '0.0.0.0', 3000
end
