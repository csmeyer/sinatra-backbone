require "sinatra/base"
require "sinatra/reloader" if development?

class SprocketsMiddleware
  attr_reader :app, :prefix, :sprockets

  def initialize(app, prefix)
    @app = app
    @prefix = prefix
    @sprockets = Sprockets::Environment.new

    yield sprockets if block_given?
  end

  def call(env)
    path_info = env["PATH_INFO"]
    if path_info =~ prefix
      env["PATH_INFO"].sub!(prefix, "")
      sprockets.call(env)
    else
      app.call(env)
    end
  ensure
    env["PATH_INFO"] = path_info
  end
end

class App < Sinatra::Base
  get '/' do
    @title = "My App!"
    slim :index
  end

  use SprocketsMiddleware, %r{/assets} do |env|
    env.append_path "assets/stylesheets"
    env.append_path "assets/templates"
    env.append_path "assets/javascripts"
  end
end
